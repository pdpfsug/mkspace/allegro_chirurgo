﻿## Materiali

| Materiale                     | Numero Pezzi                 |
| ----------------------------- | ---------------------------- |
| Adesivo isolante colorato     | 1 rotolo                     |
| Bottiglia di Plastica >1L     | 1                            |
| Pinza metallica corta         | 1                            |
| Caramelle (alla menta)        | >1/3 della bottiglia         |
| Carta bianca A4               | 2                            |
| Carta Stagnola                | >20x20cm                     |
| Cartone Spessore Medio        | >20x20cm                     |
| Coccodrillo corto             | 4                            |
| Coccodrillo lungo             | 2                            |
| Makey Makey                   | 1                            |
| Tappo bottiglia               | 1                            |


| Strumenti              |
| ---------------------- |
| Colla a caldo          |
| Computer               |
| Forbice                |
| Pennarelli colorati    |
| Stampante a colori     |
| Taglierino             |

