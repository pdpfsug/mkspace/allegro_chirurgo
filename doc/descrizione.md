### Ricavo il materiale
- **Cartone Spessore Medio**:  
  + Ricavato dalla scatola che si trovano al supermercato, chiedere ad un commess una scatola **sottili** altrimenti le classiche scatole di cartone in possesso del supermercato sono troppo spesse.
- **Carta Stagnola**:  
  + A casa tutti usano la carta stagnola, prendere il rotolo intero per non stropicciare il materiale.
- **Carta stagnola + Cartone** già incollato:  
  + Può essere ricavato da un contenitore vuoto di latte o succo di frutta, perché questo cartone (tetra-pack), la parete interna e' formata da un foglio di alluminio conduttore di elettricità.
- **Pinza metallica**:
  + Si trova in alcuni i kit per l'elettronica.
  + Comprate alla ferramenta.
  + Al posto delle pinze d'elettronica tradizionali si posso utilizzare le pinze per le sopracciglia.
- **Colla a caldo**:
  + Si può utilizzare una colla liquido tradizionale per la carta. Non utilizzare la colla solida perché andremmo ad incollare sulla plastica e non funzionerà.

----

### Costruisco la testa

1. Svuoto la bottiglia.
1. Rimuovo l'etichetta della bottiglia.
1. Stampo gli elementi della testa con la stampante:
    - [Elementi facciali](../resources/elementi_faccia.jpg): contiene la bocca, gli occhi, le orecchie e i chiodi del collo.
    - [Maschera Terrore](../resources/maschera_terrore.jpg): contiene il design della maschera terrore.
1. Ritaglio gli elementi stampati lungo il contorno. Abbiamo cosi ottenuto delle figure che utilizzeremo come riferimento.
1. Con il pennarello, facendo uso delle figure di riferimento disegno:
    - sulla bottiglia:
        + La bocca
        + 2 occhi
        + 2 orecchie
        + 2 viti del collo
    - sul cartone:
        + 2 orecchie
        + 2 viti del collo
    - sulla carta stagnola:
        + 2 orecchie
        + 2 viti del collo
1. Con le forbici ritaglio le figure appena disegnate.
1. Con la colla a caldo incollo le figure in carta stagnola sulle rispettive figure in cartone.
1. Con il taglierino ritaglio le figure disegnate sulla bottiglia in questo modo:
    - Bocca: ritaglio tutto il contorno della bocca facendo attenzione a lasciare i margini più lisci possibili.
    - Occhio: Con la punta del taglierino buco la bottiglia e ritaglio tutto intorno.
    - Orecchio: con il taglierino ritaglio solo meta orecchio facendo **attenzione** a lasciare l'altra meta (linea tratteggiata) incollata alla bottiglia. L'apertura deve avvenire verso il **retro** a 90 gradi.
    - Vite del collo: con il taglierino ritaglio solo meta vite facendo **attenzione** a lasciare l'altra meta (linea tratteggiata) incollata alla bottiglia. L'apertura deve avvenire verso il **fronte** a 90 gradi.
1. Taglio la base della bottiglia.
1. Con il taglierino ritaglio a zig zag al confine dell'apertura per formare i cappelli, facendo attenzione a non renderli troppo appuntiti.
1. Con l'adesivo isolante colorato copro le punte a zig zag e per tingere i cappelli.
1. Con la colla a caldo incollo le figure preparate in cartone sulle rispettive posizioni sulla bottiglia.
1. Con le forbici ritaglio dei rettangoli di carta stagnola di circa 2x1 cm.
1. Con la colla a caldo ripasso lungo il contorno della bocca e vado ad incollare i rettangoli di carta stagnola piegandoli verso il basso fino a ricoprire interamente il contorno della bocca.
1. Con la colla a caldo incollo la maschera sulla bottiglia facendo attenzione che gli occhi e la bocca combacino perfettamente, quindi applico la maschera partendo dalla parte laterale fino all'altra seguendo lo stato curvo della bottiglia.

----

### Costruisco il circuito

1. Collego in senso orario utilizzando i fili coccodrillo, i vari elementi della bottiglia facendo attenzione che il coccodrillo morda la carta stagnola.
    1. Con il coccodrillo lungo collego un braccio della pinza metallica al Makey Makey nella posizione **MASSA**.
    1. Con il coccodrillo lungo collego la vite del collo sinistra al Makey Makey nella posizione del tasto **SPAZIO**.
    1. Con il coccodrillo corto collegare la vite sinistra del collo all'orecchio sinistro.
    1. Con il coccodrillo corto collego l'orecchio sinistro a quello destro facendo passare il filo per il retro della bottiglia.
    1. Con il coccodrillo corto collego l'orecchio destro alla vite destra del collo.
    1. Con il coccodrillo corto collego la vite destra del collo all'apertura della bocca.
1. Con il cavo USB contenuto nel kit Makey Makey collego codesto al computer.

### Costruisco il sostegno

**TODO**

### Aggiungo decorazioni

**TODO**