# Allegro Chirurgo

![](https://i2.wp.com/makezine.com/wp-content/uploads/2014/08/mad-monster-diagram-e1409959714880.jpg?w=650&ssl=1)


## Info
L'obbiettivo e' di rubare la caramella al mostro senza toccare i bordi della bocca.


Alcuni media come esempio:
  - **Video**:
    + [Come prendo la caramella?](resources/video_1.mp4) (TODO)
    + [Cosa succede se tocco i margini della bocca?](resources/video_2.mp4) (TODO)
  - **Immagini**:
      + [Template da stampare.](resources/elementi_faccia.jpg)
      - Lista Maschere:
        + [Terrore](resources/maschera_terrore.jpg)
        + [Cattivo](resources/maschera_cattivo.jpg) (TODO)
        + [Felice](resources/maschera_felice.jpg) (TODO)

## Costruzione

Visitare la [**lista dei materiali**](doc/materiali.md).  

Scaricre il programma [Ruba la Caramella al Mostro](https://gitlab.com/pdpfsug/mkspace/ruba-la-caramella-al-mostro). [**Direct Download**](https://gitlab.com/pdpfsug/mkspace/ruba-la-caramella-al-mostro/-/archive/master/ruba-la-caramella-al-mostro-master.zip).  

Leggere la [**descrizione**](doc/descrizione.md) dell'attivita per sapere come realizzare la testa del mostro.  

## Funzionamento

Una volta che la testa e' stata costruita e il programma scaricato, riempire la bottiglia con le caramelle.  
Estrarre i file:
```
cd ~/Scaricati || cd ~/Downloads 2> /dev/null
unzip ruba-la-caramella-al-mostro-master.zip
```
Eseguire il programma sul computer: `firefox index.html`.  
Utilizzando le pinze metalliche afferra la caramella senza toccare i bordi della bocca.  